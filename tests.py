from helper_functions import *
import unittest
from test_data import next_links
import io
import sys


# Unit tests
class TestUrlExistenceCheck(unittest.TestCase):
    def test_existing_url(self):
        """
        Function should terminate normally and return None for a valid URL
        """
        url = "https://memgraph.com"
        result = url_existence_check(url)
        self.assertIsNone(result)

    def test_non_existing_url(self):
        """
        Function should raise an error for an invalid URL
        """
        url = "https://nonexistent.at"
        self.assertRaises(WebsiteNotFound, url_existence_check, url)


class TestNodeExistenceCheck(unittest.TestCase):
    def test_existing_node(self):
        """
        Function should terminate normally and return None for a URL that exists in the database
        """
        base_url = "https://www.unittesturl.com"
        query = "CREATE (w:Website {{name:'{}'}})".format(base_url)
        cursor.execute(query)
        result = node_existence_check(base_url)
        self.assertIsNone(result)

    def test_non_existing_node(self):
        """
        Function should raise an error if a node is not in the database
        """
        base_url = "https://www.notinthedatabase.com"
        self.assertRaises(WebsiteNotFound, node_existence_check, base_url)


class TestUrlExpander(unittest.TestCase):
    def test_scrape(self):
        """
        Given a valid URL, the function should return a non-empty list of links on the URL
        """
        url = "https://memgraph.com"
        result = url_expander(url, url)
        self.assertGreater(len(result), 0)


# Integration tests
class TestGraphConstructor(unittest.TestCase):
    def test_database_write(self):
        """
        Function should create a graph in the database that contains all the links from a given test URL
        """
        url = "https://memgraph.com"
        next_links_list = next_links
        graph_construct(url, 1)
        query = "MATCH a = (w:Website) UNWIND (nodes(a)) as rows RETURN rows.name"
        cursor.execute(query)
        result = cursor.fetchall()
        result = [entry[0] for entry in result]
        for entry in next_links_list:
            self.assertIn(entry, result)
        cursor.execute("MATCH (a) -[r] -> () DETACH DELETE a, r")
        cursor.execute("MATCH (a) DELETE a")


class TestShortestPathFinder(unittest.TestCase):
    def test_shortest_path(self):
        """
        Function should print a path of correct length for fixed URLs in database
        """
        graph_construct("https://memgraph.com", 2)
        captured_output = io.StringIO()
        sys.stdout = captured_output
        find_shortest_path("https://memgraph.com", "https://cloud.memgraph.com/login")
        sys.stdout = sys.__stdout__
        path_length = int(captured_output.getvalue().split(" clicks")[0][-1])
        self.assertEqual(path_length, 2)


if __name__ == '__main__':
    unittest.main()