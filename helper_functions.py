import mgclient
from bs4 import BeautifulSoup
import requests

# establish a connection to the database
connection = mgclient.connect(host='172.17.0.2', port=7687)
cursor = connection.cursor()


# define custom exceptions
class WebsiteNotFoundError(Exception):
    pass


class MissingArguments(Exception):
    pass


class ShortestPathNotFound(Exception):
    pass


def url_existence_check(url):
    """
    Checks if a HTTP request can be made to the provided URL
    :param url: string
        url to check
    :return: None
    :raises: WebsiteNotFound
        if the status code of the request is 404, or a request exception occurs
    """
    try:
        response = requests.get(url)
        if response.status_code == 404:
            raise WebsiteNotFound("WebsiteNotFoundError(URL)")
    except (requests.exceptions.ConnectionError, requests.exceptions.InvalidURL, requests.exceptions.MissingSchema):
        raise WebsiteNotFound("WebsiteNotFoundError(URL)")


def node_existence_check(url):
    """
    Checks if a node with a name property equal to the provided URL exists in the database
    :param url: string
        url to check
    :return: None
    :raises WebsiteNotFound
        if the result of the query is an empty list
    """
    global cursor
    query = "MATCH (a:Website {{name: '{}'}}) RETURN a".format(url)
    cursor.execute(query)
    if not cursor.fetchall():
        raise WebsiteNotFound("WebsiteNotFoundError(URL)")


def url_expander(web_url, base_url):
    """
    Outputs a list of links contained on a particular URL
    :param web_url: string
        URL where the links are extracted
    :param base_url: string
        Base url in case relative links are used
    :return: list
        A list of unique URLs that can be reached from the URL provided as input
    """
    item = requests.get(web_url)
    soup = BeautifulSoup(item.content, "html.parser")
    urls = soup.findAll("a")
    result = [base_url + uri["href"] if uri["href"].startswith("/") else uri["href"] for uri in urls]
    return list(set(result))


def graph_construct(base_url, depth):
    """
    Given a URL, writes a graph of child pages (with specified depth) into Memgraph
    A queue is used in construction, and visited nodes are tracked and prevented from
    being include multiple times
    :param base_url: string
        starting point for the graph construction
    :param depth: integer
        maximum depth of the graph
    :return: None
    """
    current_urls = [base_url]
    next_urls = []
    visited = []
    global cursor

    query = "CREATE (w:Website {{name:'{}'}})".format(base_url)
    cursor.execute(query)

    for i in range(depth):

        while current_urls:
            scrape_url = current_urls.pop(0)
            if scrape_url not in visited:
                try:
                    next_connections = url_expander(scrape_url, base_url)
                    next_urls += next_connections
                    visited.append(scrape_url)
                    for conn in next_connections:
                        if conn not in visited:
                            query = "MATCH (w:Website {{name:'{}'}}) RETURN w".format(conn)
                            cursor.execute(query)
                            if not cursor.fetchall():
                                query = """
                                MATCH (w1:Website {{name:'{}'}})
                                CREATE (w1)-[:LEADS_TO]->(w2:Website {{name:'{}'}})
                                """.format(scrape_url, conn)
                                cursor.execute(query)
                            else:
                                query = """
                                MATCH (w1:Website {{name:'{}'}})
                                MATCH (w2:Website {{name:'{}'}})
                                CREATE (w1)-[:LEADS_TO]->(w2)
                                """.format(scrape_url, conn)
                                cursor.execute(query)

                except KeyError:
                    visited.append(scrape_url)

        current_urls = next_urls.copy()
        next_urls = []


def find_shortest_path(begin, end):
    """
    Prints the shortest path between two URLs (nodes in graph)
    :param begin: string
        URL to find the path from
    :param end: string
        URL to find the path to
    :return: None
    :raises: ShortestPathNotFound
        if the path does not exist between two input nodes
    """
    global cursor

    query = """
    MATCH p = (a:Website {{name: '{}'}})-[r:LEADS_TO * bfs]-(b:Website {{name: '{}'}})
    UNWIND (nodes(p)) AS rows
    RETURN rows.name;
    """.format(begin, end)

    cursor.execute(query)
    shortest_path = cursor.fetchall()

    if not shortest_path:
        raise ShortestPathNotFound("ShortestPathNotFoundError")

    print("Shortest Path: {} clicks".format(len(shortest_path) - 1))
    for i, url in enumerate(shortest_path):
        print("{} - {}".format(i, url[0]))