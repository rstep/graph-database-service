from sys import argv, exit
import argparse
from helper_functions import *


if __name__ == "__main__":
    # at least two arguments have to be supplied to the main function
    # a custom exception is raised if not the case
    try:
        mode = argv[1]
        start_url = argv[2]
    except IndexError:
        raise MissingArguments("Arguments missing.")

    # check that the first argument is a valid operation
    if mode not in ["network", "path"]:
        print("Unknown operation. Please specify either network or mode.")
        exit()

    # parse the arguments in network mode and call functions required to write a graph
    if mode == "network":
        parser = argparse.ArgumentParser()
        parser.add_argument("--depth", "-d", help="Scrape depth", default=2)
        args = parser.parse_args(argv[3:])
        d = int(args.depth)
        url_existence_check(start_url)

        graph_construct(start_url, d)
        connection.commit()

    # check that 3rd argument exists in path mode, and call functions to calculate the shortest path
    elif mode == "path":
        try:
            end_url = argv[3]
        except IndexError:
            raise MissingArguments("Arguments missing.")

        node_existence_check(start_url)
        node_existence_check(end_url)
        find_shortest_path(start_url, end_url)